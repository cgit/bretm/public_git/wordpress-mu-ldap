<?php
/*
Plugin Name: LDAP User Info Import Plugin for WPMU
Plugin URI: http://www.redhat.com/
Description: A simplistic plugin to pull in user information from LDAP after a blank account has been created post HTTP Auth
Version: 0.1
Author: Bret McMillan (bretm@redhat.com)

Copyright (C) 2008 Red Hat, Inc.

This is free software, licensed to you under the GNU General Public
License, version 2 (GPLv2). A copy of GPLv2 is available at 
http://www.gnu.org/licenses/old-licenses/gpl-2.0.html .

This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

if (! class_exists('LDAPImportPlugin')) {

  class LDAPImportPlugin {

    function LDAPImportPlugin() {
      add_action('init', array(&$this, 'init'));
      add_action('wpmu_new_user', array(&$this, 'ldap_newuser_info_import'));
      add_action('admin_menu', array(&$this, 'admin_menu'));
    }

    function init() {
      if ( !get_site_option('ldap_server') && current_user_can('manage_options')) {
	add_site_option('ldap_server', 'ldap1.foo.com', 'The hostname of the LDAP server to read from');
	add_site_option('ldap_dn', 'dc=foo,dc=com', 'The LDAP base DN to use');
      }
    }

    function admin_menu() {
      $cur_user = wp_get_current_user();
      $cached_user = wp_cache_get($cur_user->ID, 'users');
      if (function_exists('add_submenu_page') &&
	  is_site_admin($cached_user->user_login)) {
	add_submenu_page('wpmu-admin.php', 'LDAP Import', 'LDAP Import', 9, basename(__FILE__), array(&$this, 'render_options'));
      }
    }

    function render_options() {
      if( ! is_site_admin() ) {
	wp_die( __('<p>You do not have permission to access this page.</p>') );
      }

      if ($_POST['ldapOptionsSave']) {
	update_site_option('ldap_server', $_POST['ldap_server']);
	update_site_option('ldap_dn', $_POST['ldap_dn']);
        ?>
	  <div id="message" class="updated fade"><p><?php _e('Options saved!'); ?></p></div><?php
      }
      $ldap_server = get_site_option('ldap_server');
      $ldap_dn = get_site_option('ldap_dn');
?>
<div class="wrap">
  <h2>LDAP User Information Import Options</h2>
  <form method="post">
    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="ldap_server,ldap_dn" />
    <?php if (function_exists('wp_nonce_field')): wp_nonce_field('update-options'); endif; ?>

    <fieldset class="options">
      <table class="editform optiontable">
        <tr valign="top">
          <th scope="row"><label for="ldap_server">LDAP Server</label></th>
          <td>
            <input type="text" name="ldap_server" id="ldap_server" value="<?php echo htmlspecialchars($ldap_server) ?>" size="50" />
          </td>
        </tr>
        <tr valign="top">
          <th scope="row"><label for="ldap_dn">LDAP Base DN</label></th>
          <td>
            <input type="text" name="ldap_dn" id="ldap_dn" value="<?php echo htmlspecialchars($ldap_dn) ?>" size="20" />
          </td>
        </tr>
      </table>
    </fieldset>
    <p class="submit">
      <input type="submit" name="ldapOptionsSave" value="Update Options &raquo;" />
    </p>
  </form>
</div>
<?php
    }

    function ldap_newuser_info_import( $user_id ) {
      global $wpdb;

      $user = new WP_User($user_id);

      $principal = preg_split('/\@/', $user->user_login, -1, PREG_SPLIT_NO_EMPTY);
      $username = $principal[0];

      $ldap_server = get_site_option("ldap_server");
      $ldap_dn = get_site_option("ldap_dn");

      if ( ! $ldap_server || ! $ldap_dn )
        die("LDAP options not set");
  
      $ds = ldap_connect($ldap_server);
      $filter ="(uid=$username)";
      $justthese = array("sn", "givenname", "mail");

      $sr = ldap_search($ds, $ldap_dn, $filter, $justthese);

      $info = ldap_get_entries($ds, $sr);


      if ($info["count"] <= 0) {
        // bad things happened...
        die("User authenticated, but not found in LDAP!\n");
      }

      update_usermeta($user->ID, 'first_name', $wpdb->escape($info[0]['givenname'][0]));
      update_usermeta($user->ID, 'last_name', $wpdb->escape($info[0]['sn'][0]));
      wp_update_user(array('ID' => $user_id, 'user_email' => $info[0]['mail'][0], 'display_name' => $username));

      return true;
    }
  }

  $ldap_userinfo_import_plugin = new LDAPImportPlugin();
 }
?>
